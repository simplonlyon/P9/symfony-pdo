<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ColorType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use App\Entity\Bicycle;


class BicycleType extends AbstractType {

    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options)
    {
        $builder->add("brand", TextType::class)
        ->add("color", ColorType::class)
        ->add("gearNb", IntegerType::class)
        ->add("electric", CheckboxType::class, [
            "label" => "Is it electrical ?",
            "required" => false
        ]);
    }

    public function configureOptions(\Symfony\Component\OptionsResolver\OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            "data_class" => Bicycle::class
        ]);
    }


}
<?php

namespace App\Repository;

/**
 * On crée une classe qui nous permettra de récupérer une connexion à la
 * base de données depuis n'importe quelle autre endroit
 */
class ConnectionUtil
{
    /**
     * Cette méthode permet de récupérer une connexion pdo vers la bdd.
     * On a choisit un peu arbitrairement d'en faire une méthode static,
     * c'est à dire qu'il ne sera pas nécessaire d'instancier la classe
     * ConnectionUtil pour déclencher la méthode getConnection, à la 
     * place on fera NomDeLaClasse::nomDeLaMethode() ou dans ce cas :
     * ConnectionUtil::getConnection()
     */
    public static function getConnection(): \PDO
    {

        /**
         * On crée une connexion à notre base de données en utilisant PDO.
         * On lui indique la localisation du serveur bdd (le host), le 
         * nom de la bdd (dbname), le username et le password pour s'y 
         * connecter et enfin des options. Ici, on dit à PDO de planter
         * si jamais il a des problèmes de connexion ou autre
         * On fait en sorte d'aller chercher les différentes informations
         * de connexion dans les variables d'environnements afin de
         * pouvoir les modifier facilement et de ne pas les commit
         * sur gitlab pour des raisons de sécurité. Elles doivent
         * se trouver dans le fichier .env.local à la racine du projet
         */
        return  new \PDO(
            "mysql:host=" . $_ENV["DATABASE_HOST"] . ";dbname=" . $_ENV["DATABASE_NAME"],
            $_ENV["DATABASE_USERNAME"],
            $_ENV["DATABASE_PASSWORD"],
            [\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION]
        );
    }
}

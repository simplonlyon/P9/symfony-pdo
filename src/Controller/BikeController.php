<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\BicycleType;
use App\Entity\Bicycle;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\BicycleRepository;


class BikeController extends AbstractController {

    //Ici, on injecte notre DAO dans notre Route afin de pouvoir utiliser
    //ses méthodes d'accès aux données
    /**
     * @Route("/add-bike", name="add_bike")
     */
    public function index(Request $request, BicycleRepository $repo) {
        
        // dump($repo->find(100));

        // $bike = $repo->find(1);
        // $bike->brand = "follis";
        // $repo->update($bike);
        // $repo->remove($bike);
        
        $bicycle = new Bicycle();
        $form = $this->createForm(BicycleType::class, $bicycle);
        
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid()) {
            /**
             * Si le formulaire est submit et valide, alors on déclenche
             * la méthode add de notre repository pour faire persister
             * l'instance de Bicycle liée à notre formulaire
             */
            $repo->add($bicycle);
            dump($bicycle);
        }
        
        return $this->render("add-bike.html.twig", [
            "form" => $form->createView(),
            "bike" => $bicycle
        ]);
    }
    /**
     * @Route ("/", name="all_bikes")
     */
    public function showBikes(BicycleRepository $repo){
        return $this->render("all-bikes.html.twig", [
            "bikes" => $repo->findAll()
        ]);
    }

    /*
    Ici, on utilise une route avec un paramètre dans son path.
    Celui ci est représenté par son nom entre accolades (ici {id})
    Ce paramètre sera fournie lorsque l'utilisateur.ice finale tapera 
    l'url dans la barre d'adresse de son navigateur et on pourra 
    le récupérer en mettant un argument avec le même nom dans notre
    fonction de route (ici $id)
    */
    /**
     * @Route ("/show-bike/{id}", name="show_bike")
     */
    public function oneBike(BicycleRepository $repo, int $id){
        //On se sert de l'argument de la route pour aller chercher
        //un vélo spécifique avec la méthode find du repository
        return $this->render("show-bike.html.twig", [
            "bike" => $repo->find($id)
        ]);
    }
}
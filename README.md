# Symfony PDO
Un projet utilisant symfony et PDO

## How to use
1. Cloner le projet
2. Faire un `composer install`
3. Créer une base de données `symfony_pdo` dans mysql (d'une manière ou d'une autre)
4. Importer la base de données avec `mysql -u [votre username] -p symfony_pdo < database.sql`
5. Lancer le serveur `bin/console server:run` et y accéder sur http://localhost:8000